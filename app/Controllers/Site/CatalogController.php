<?php

namespace App\Controllers\Site;


use App\Models\Articles;
use App\Models\Category;
use App\Repositories\ModelsFactory;
use App\Repositories\ProductImages;

class CatalogController extends SiteController
{
    private $class;

    // Показать каталог по одной категории товаров
    // Из URL приходит транслитерация категории, соответствующие классы для категорий - названы на английском языке
    public function show($categoryUrl, $page = 1, $orderName = 'price', $orderType = 'desc')
    {
        $categoryUrl = htmlentities($categoryUrl);

        // Получаем данные для шаблона из таблицы категорий
        $category = Category::getCategory($categoryUrl);
        $this->data['head_title'] = $category['title'];
        $this->data['name_category'] = $category['name'];
        // Получаем название класса категории, соответствующее транслитерации
        $this->class = $category['category_class'];

        // Рендер html для фильтра в левом сайдбаре - содержимое фильтра зависит от категории, поэтому
        // вставляем в шаблон как переменную
        $this->data['filter'] = static::getTwig()->render("include/filter_$this->class.html.twig");

        // Коллекция товаров для каталога (основная секция в шаблоне) - массив данных для шаблона.
        // Параметры для выборки и сортировки не передаем - они в GET и POST
        $collection = ModelsFactory::getListByCategory($this->class, $page, $orderName, $orderType);

        // Добавим фотографии в коллекцию товаров
        $collection = ProductImages::addFotoCollection($this->class,$collection);

//        print_r($collection);die;

        // Рендерим html для каталога товаров, будем вставлять в шаблон как переменную
        $this->data['catalog'] = static::getTwig()->render('include/catalog_block.html.twig', [
                'collection' => $collection,
                'category' => $category['category_translit'], // - берем из БД, а не из запроса - для безопасности
                'pathPhotoGoods' => PATH_PHOTO_GOODS
        ]);

        // Получим seo статью под каталогом товаров по названию класса товаров
        $this->data['article'] = Articles::getArticleByClass($this->class);

        // Передаем все куски html и переменные в основной шаблон страницы категорий
        echo static::getTwig()->render('site/category.html.twig', [
            'data' => $this->data
        ]);
    }
}