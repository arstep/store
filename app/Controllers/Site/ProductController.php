<?php

namespace App\Controllers\Site;


use App\Models\Category;
use App\Repositories\ModelsFactory;
use App\Repositories\ProductImages;

class ProductController extends SiteController
{
    private $class;


    public function show($categoryUrl,$id)
    {
        $categoryUrl = htmlentities($categoryUrl);

        // Данные для шаблона из таблицы категорий
        $category = Category::getCategory($categoryUrl);
        $this->data['name_category'] = $category['name'];
        $this->data['url_category'] = $category['category_translit'];
        $this->class = $category['category_class'];

        // Определим класс модели по категории
        $className = ModelsFactory::getClassName($this->class);

        // Получим массив данных по товару
        $this->data['product'] = $className::getProductById($id);

        $this->data['head_title'] = $this->data['product']['name'];

        // Получим фотографии товара по артикулу и добавим в массив данных продукта
        $articul = $this->data['product']['articul'];
        $this->data['product']['photo'] = ProductImages::getFotoByArticul($this->class,$articul);

//        print_r($this->data['product']);

        // Рендерим html правый инфо-блок товара
        $info_product = static::getTwig()->render('include/product_block.html.twig', [
           'data' => $this->data,
        ]);

        // Все передаем в основной шаблон страницы товара и выводим
        echo static::getTwig()->render('site/product.html.twig', [
            'data' => $this->data,
            'pathPhotoGoods' => PATH_PHOTO_GOODS,
            'info_product' => $info_product,
        ]);
    }




    // Показать товар по артикулу - НЕ НУЖНО - УДАЛИТЬ!!!!!!!!!!!
    public function showArticul($categoryUrl,$articul)
    {
        $categoryUrl = htmlentities($categoryUrl);

        // Данные для шаблона из таблицы категорий
        $category = Category::getCategory($categoryUrl);
        $this->data['name_category'] = $category['name'];
        $this->data['url_category'] = $category['category_translit'];
        $this->class = $category['category_class'];

        // Определим класс модели по категории
        $className = ModelsFactory::getClassName($this->class);

        // Получим массив данных по товару (по артикулу)
        $this->data['product'] = $className::getProductByArticul($articul);

        // Получим фотографии товара по артикулу и добавим в массив данных продукта
        $this->data['product']['photo'] = ProductImages::getFotoByArticul($this->class,$articul);

        echo static::getTwig()->render('site/product.html.twig', [
            'data' => $this->data,
            'pathPhotoGoods' => PATH_PHOTO_GOODS
        ]);
    }
}