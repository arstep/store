<?php

namespace App\Controllers\Site;


use App\Controllers\Controller;
use App\Models\Cart;
use App\Models\Category;

class SiteController extends Controller
{

    public function __construct()
    {
//        $this->data['categories'] = Category::getListCategory();
        $this->data['cartHtml'] = $this->cartBlockHtml();
    }

    // Рендерим содержимое козины
    public function cartBlockHtml()
    {
        return Cart::getRenderCart();
    }

}