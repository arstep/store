<?php
namespace App\Controllers;

use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;

abstract class Controller
{
    protected $data;
    private static $twig;

    // Singleton Twig
    public static function getTwig()
    {
        if (empty(self::$twig)){
            $loader = new Twig_Loader_Filesystem(PATH_VIEWS);

            self::$twig = new Twig_Environment($loader, array(
                'cache' => PATH_VIEWS . 'compilation_cache/',
                'auto_reload' => true,
                'debug' => true,
            ));

            self::$twig->addExtension(new Twig_Extension_Debug());
        }
        return self::$twig;
    }

}