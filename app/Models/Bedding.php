<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 08.05.2017
 * Time: 12:59
 */

namespace App\Models;


use App\Repositories\ProductImages;
use Db;
use PDO;

class Bedding extends BaseModel
{
    private static $name = 'bedding';


    // Получить массив товаров по id (без фото)
    public static function getProductById($id)
    {
        $db = Db::getConnection();

        // Получаем все общие характеристики для артикула
        $query = "SELECT 
                bedding.articul, 
                bedding.name,
                material.name as material_name,
                material.title as material_title,
                material.description as material_description,
                size.name as size_name,
                size.title as size_title,
                size.description as size_description,
                bedding.density, 
                pillowcases_size.name as pillowcases_name, 
                pillowcases_size.title as pillowcases_title, 
                brand.name as brand_name,
                brand.title as brand_title,
                bedding.price,
                bedding.price_old,
                bedding.price_vendor,
                region.name as region_name,
                region.title as region_title,
                status,  
                bedding.description  
                FROM bedding 
                LEFT JOIN size ON bedding.size = size.id 
                LEFT JOIN material ON bedding.material = material.id 
                LEFT JOIN pillowcases_size ON bedding.pillowcases = pillowcases_size.id 
                LEFT JOIN brand ON bedding.brand = brand.id 
                LEFT JOIN region ON bedding.region = region.id 
                WHERE bedding.id = :id 
                GROUP BY articul 
                ORDER BY price ASC";

        $stm = $db->prepare($query);
        $stm->bindValue(':id',$id);

        $stm->execute();
        $product = $stm->fetch(PDO::FETCH_ASSOC);

        // Получаем размеры и цены из той же таблицы для всего артикула товара
        $articul = $product['articul'];

        $query = "SELECT 
                bedding.id,
                bedding.price,
                bedding.price_old,
                bedding.price_vendor,
                size.name as size_name,
                size.title as size_title,
                size.description as size_description 
                FROM bedding 
                LEFT JOIN size ON bedding.size = size.id 
                WHERE bedding.articul = :articul 
                ORDER BY price ASC";

        $stm = $db->prepare($query);
        $stm->bindValue(':articul',$articul);

        $stm->execute();
        $prices = $stm->fetchAll(PDO::FETCH_ASSOC);

        // Добавляем массив цен и размеров к основному массиву
        $product['prices'] = $prices;

        return $product;
    }










    // Получить массив товаров по артикулу (без фото)  - НЕНУЖНО - УДАЛИТЬ!!!!!!!!!!!
    public static function getProductByArticul($articul)
    {
        $db = Db::getConnection();

        // Получаем все общие характеристики для артикула
        $query = "SELECT 
                bedding.articul, 
                bedding.name,
                material.name as material_name,
                material.title as material_title,
                material.description as material_description,
                bedding.density, 
                pillowcases_size.name as pillowcases_name, 
                pillowcases_size.title as pillowcases_title, 
                brand.name as brand_name,
                brand.title as brand_title,
                region.name as region_name,
                region.title as region_title,
                status,  
                bedding.description  
                FROM bedding 
                LEFT JOIN material ON bedding.material = material.id 
                LEFT JOIN pillowcases_size ON bedding.pillowcases = pillowcases_size.id 
                LEFT JOIN brand ON bedding.brand = brand.id 
                LEFT JOIN region ON bedding.region = region.id 
                WHERE bedding.articul = :articul 
                GROUP BY articul 
                ORDER BY price ASC";

        $stm = $db->prepare($query);
        $stm->bindValue(':articul',$articul);

        $stm->execute();
        $product = $stm->fetch(PDO::FETCH_ASSOC);

        // Получаем размеры и цены из той же таблицы для каждого id товара
        $query = "SELECT 
                bedding.id,
                bedding.price,
                bedding.price_old,
                bedding.price_vendor,
                size.name as size_name,
                size.title as size_title,
                size.duvet_cover as size_duvet_cover 
                size.bedsheet as size_bedsheet 
                FROM bedding 
                LEFT JOIN size ON bedding.size = size.id 
                WHERE bedding.articul = :articul 
                ORDER BY price ASC";

        $stm = $db->prepare($query);
        $stm->bindValue(':articul',$articul);

        $stm->execute();
        $prices = $stm->fetchAll(PDO::FETCH_ASSOC);

        // Добавляем массив цен и размеров к основным данным артикула
        $product['prices'] = $prices;

        return $product;
    }




    // Возвращает массив для шаблона блока списка товаров по категории с сортировкой и учетом пагинациии
    public static function getListArticuls($page, $orderName, $orderType)
    {
        $offset = ($page - 1) * GOODS_ON_PAGE_CATALOG;
        $orderTypes = ['asc', 'desc'];
        $orderNames = ['price', 'material'];

        if (!in_array($orderType, $orderTypes)) {
            throw new \Exception('Неправильные входные данные в запросе (параметр $orderType');
        }
        if (!in_array($orderName, $orderNames)) {
            throw new \Exception('Неправильные входные данные в запросе (параметр $orderName');
        }

        $db = Db::getConnection();

        $order = 'ORDER BY price ASC';

        // Получаем основные данные по товарам с сортировками и условиями. Данные сгруппированы двухмерным массивом,
        // в котором ключи - это артикулы, а вложенные массивы - значения всех остальных полей
        $query = "SELECT 
                bedding.articul, 
                bedding.id, 
                bedding.name,
                size.name as size_name,
                size.title as size_title,
                material.name as material_name,
                material.title as material_title,
                brand.name as brand_name,
                brand.title as brand_title,
                pillowcases_size.name as pillowcases_name, 
                pillowcases_size.title as pillowcases_title,
                bedding.price,
                bedding.price_old,
                status 
                FROM bedding 
                LEFT JOIN size ON bedding.size = size.id 
                LEFT JOIN material ON bedding.material = material.id 
                LEFT JOIN brand ON bedding.brand = brand.id 
                LEFT JOIN pillowcases_size ON bedding.pillowcases = pillowcases_size.id 
                GROUP BY articul "
                . $order
                . " LIMIT " . GOODS_ON_PAGE_CATALOG
                . " OFFSET " . $offset;
        $collection = $db->query($query)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_UNIQUE);

        return$collection;
    }


}