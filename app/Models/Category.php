<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 05.05.2017
 * Time: 20:39
 */

namespace App\Models;

use Db;
use PDO;

class Category extends BaseModel
{

    // Данные для шаблона по запрошенной категории
    // Из URL приходит транслитерация категории, соответствующие классы для категорий - названы на английском языке
    // из-за SEO
    public static function getCategory($categoryUrl)
    {
        $db = Db::getConnection();

        $query = "SELECT `id`,`name`,`title`,`category_translit`,`category_class`,`image` 
                  FROM `category` WHERE `category_translit` = :category_translit LIMIT 1";
        $stm = $db->prepare($query);
        $stm->bindValue(':category_translit',$categoryUrl);

        if ($stm->execute()) {
            return $stm->fetch(PDO::FETCH_ASSOC);
        }
    }

}