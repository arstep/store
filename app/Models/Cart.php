<?php

namespace App\Models;


use App\Controllers\Controller;

class Cart
{
    private static $buy = false;

    public static function getRenderCart()
    {

        if (isset($_SESSION['buy']) && !empty($_SESSION['buy'])){

            self::$buy = $_SESSION['buy'];
        }

        return Controller::getTwig()->render('include/cart_block.html.twig',['buy' => self::$buy]);
    }
}