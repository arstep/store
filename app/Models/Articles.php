<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 11.05.2017
 * Time: 21:38
 */

namespace App\Models;


use Db;
use PDO;

class Articles extends BaseModel
{

    // Получить seo-текст для каталога по имени класса товара
    public static function getArticleByClass($title)
    {
        $db = Db::getConnection();

        $belong = 'seo_'.$title;

        $query = "SELECT * FROM articles WHERE belong = '$belong'";

        return $db->query($query)->fetch(PDO::FETCH_ASSOC);
    }

}