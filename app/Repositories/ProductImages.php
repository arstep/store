<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 03.04.2017
 * Time: 21:26
 */

namespace App\Repositories;


use Db;
use PDO;

class ProductImages
{
    // Получить массив фотографий по артикулу
    public static function getFotoByArticul($className,$articul)
    {
        $db = Db::getConnection();

        $table = $className.'_img';

        $query = "SELECT * FROM $table WHERE articul = :articul ORDER BY sort ASC";

        $stm = $db->prepare($query);
        $stm->bindValue(':articul',$articul, PDO::PARAM_INT);

        $stm->execute();
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);

        return self::addFilename($result,$articul);
    }





    // Добавить фотографии в коллекцию товаров. Принимает аргумент - название класса товаров и сам массив товаров.
    // Возвращает массив товаров с вложенными массивами фотографий в них.
    public static function addFotoCollection($className,array $collection)
    {
        $db = Db::getConnection();

        // Создадим массив по ключам (по артикулам) из коллекции товаров.
        $articuls = array_keys($collection);
        // Преобразуем массив в строку с разделителями - запятыми
        $articuls = implode(',', $articuls);

        $table = $className.'_img';

        $query = "SELECT articul,id,type,sort FROM $table WHERE articul IN ($articuls) ORDER BY sort ASC";

        $photos = $db->query($query)->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);

        // Сразу формируем имя файла каждой фотографии
        foreach ($photos as $articul => $value){
            foreach ($value as $photo => $data){
                $name = $articul.'-'.$data['id'].'.'.$data['type'];
                $photos[$articul][$photo]['name'] = $name;
            }
        }

        // Добавляем фотографии в коллекцию товаров. Перебираем артикулы.
        foreach ($collection as $articul => $value){
            $collection[$articul]['photo'] = $photos[$articul];
        }

        return $collection;
    }








    // Обработать массив фотографий - сформировать в нем поле - имя файла
    public static function addFilename(array $photos,$articul)
    {
        foreach ($photos as $num => $photo){
            $name = $articul.'-'.$photo['id'].'.'.$photo['type'];
            $photos[$num]['filename'] = $name;
        }

        return $photos;
    }

}