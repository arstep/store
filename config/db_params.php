<?php
defined('verification') or die('Доступ закрыт');

return array(
    'host' => 'localhost',
    'dbname' => 'store',
    'user' => 'root',
    'password' => 'password',
);