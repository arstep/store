<?php
// Включение тестового режима
define('TESTMODE', true);

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('verification', true);

// установка временной локали
setlocale(LC_ALL, "russian");

// Пути для подключения файлов видов
define('PATH_VIEWS', ROOT. '/views/');

// Путь для запросов фотографий товаров
define('PATH_PHOTO_GOODS','images_goods');

// Количество товаров на одной странице каталога
define('GOODS_ON_PAGE_CATALOG', 18);