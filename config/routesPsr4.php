<?php
defined('verification') or die('Доступ закрыт');

return array(


    // Каталог товаров
//    'catalog/([a-z-]+)/art([a-z0-9-]+)' => 'Site\ProductController@showArticul/$1/$2', // страница артикула /catalog/bedding/art1001
    'catalog/([a-z-]+)/page-([0-9-]+)' => 'Site\CatalogController@catalog/$1/$2', //action catalog($id, @page) в Site/CatalogController
    'catalog/([a-z-]+)/([0-9-]+)' => 'Site\ProductController@show/$1/$2', // страница товара /catalog/bedding/1001
    'catalog/([a-z-]+)' => 'Site\CatalogController@show/$1', // /catalog/postelnoe-bele

    // Главная страница
    '' => 'Site\IndexController@index', // actionIndex в IndexController
);