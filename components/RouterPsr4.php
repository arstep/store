<?php


class RouterPsr4
{
    private $routes;
    private $controller;
    private $method;
    private $params = [];
    private $controller404 = 'ErrorController';
    private $method404 = 'action404';


    public function __construct()
    {
        $routesPath = ROOT . '/config/routesPsr4.php';
        $this->routes = require_once $routesPath;
    }

    public function run()
    {
        $this->dispatcher();

        // Полное квалифицированное имя класса контроллера
        $nsController = '\App\Controllers\\' . $this->controller;

        if (class_exists($nsController)) {

            $controller = new $nsController;

            if (method_exists($controller, $this->method)) {

                call_user_func_array([$controller, $this->method], $this->params);
            }
        }
    }

    /*
     * Обработка строки запроса. Устанавливает все элементы необходимые переменные для запуска маршрута
     * */
    private function dispatcher()
    {
        $requestedUrl = $this->getUri();

        // Ищем этот запрос в массиве маршрутов
        foreach ($this->routes as $pattern => $realPath) {

            // Если шаблон из массива совпал со строкой запроса целиком
            if (preg_match('#^' . $pattern . '$#', $requestedUrl)) {

                // Меняем строку запроса на соответствующий реальный маршрут с подстановкой найденных в строке параметров
                $realUrl = preg_replace('#^' . $pattern . '$#', $realPath, $requestedUrl);

                // Разбиваем для отделения контроллера с Namespace
                $segments = explode('@', $realUrl);

                $this->controller = $segments[0];

                // Разбиваем вторую часть
                $arr = explode('/', $segments[1]);

                // Берем обязательный первый член из массива - это метод
                $this->method = array_shift($arr);

                // Все что осталось в массиве - это аргументы. Пустой массив также необходимый параметр для запуска.
                $this->params = $arr;

                return true;
            }
        }

        $this->controller = $this->controller404;
        $this->method = $this->method404;

        return true;
    }

    /*
     * Берет Uri из запроса
     * */
    private function getUri()
    {
        $uri = explode('?', $_SERVER["REQUEST_URI"])[0];
        return $requestedUrl = trim($uri, '/');
    }
}