<?php
//defined('verification') or die('Доступ закрыт');

class Db
{
    private static $_instance;


    public static function getConnection()
    {

        if ( ! (self::$_instance instanceof self)){

            $paramsPath = ROOT .'/config/db_params.php';
            $params = include ($paramsPath);

            $dsn = "mysql:host={$params['host']}; dbname={$params['dbname']}";

            self::$_instance  = new PDO($dsn, $params['user'], $params['password']);
        }

        return self::$_instance;
    }
}