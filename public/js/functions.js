window.onload = function () {

    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'side': 'left'
    });
    document.querySelector('.js-slideout-toggle').addEventListener('click', function () {
        slideout.toggle();
    });
    document.querySelector('.menu').addEventListener('click', function (eve) {
        if (eve.target.nodeName === 'A') {
            slideout.close();
        }
    });

//-----------------------------
    $('.splLink').click(function () {
        $(this).parent().children('.splCont').toggle('normal');
        return false;
    });

    $('.splLinkEdit').click(function () {
        var edit = $(this).attr('data-edit');
        var old = $(this).html();
        $(this).html(edit);
        $(this).attr('data-edit',old);
        $(this).parent().children('.splContEdit').toggle('normal');
        return false;
    });

// ----------------------------------------------------
    $('.minus').click(function () {
        var input = $(this).parent().find('input');
        var count = parseInt(input.val()) - 1;
        count = count < 1 ? 1 : count;
        input.val(count);
        input.change();
        return false;
    });
    $('.plus').click(function () {
        var input = $(this).parent().find('input');
        input.val(parseInt(input.val()) + 1);
        input.change();
        return false;
    });
/*    $('.quantity input').change(function () {
        var value = this.value;
        var price = $(this).closest('.desc_item').find('.count_price').html();
        $(this).closest('.desc_item').find('.total_item').html(value * price);
    })*/

//---------Выбор отмеченных checkbox исключением(или отмечен любой или какие-то выбранные, но не все сразу)-------------------------------------------------------------------------

    $('#bedding_filter_form input[type=checkbox]').click(function () {
        if ($(this).hasClass('checkbox_default')){
            $(this).closest('ul').find('input:not(.checkbox_default)').removeAttr('checked')
        }
        else{
            $(this).closest('ul').find('input.checkbox_default').removeAttr('checked')
        }
    })

//Ajax фильтра sidebar------------------------------------------------
    if ($(window).width() > 767)
        $('#bedding_filter_form input').click(function () {
            var data = $('#bedding_filter_form').serializeArray();
            // console.log(data);
            // отправка Ajax с запросом отфильтрованных данных
        })

};

//-------------------------------
var title = document.getElementsByClassName('clampestitle');
var text = document.getElementsByClassName('clampestext');
Array.prototype.forEach.call(title,function (item) {
    $clamp(item, {clamp: 2, useNativeClamp: false});
});
Array.prototype.forEach.call(text,function (item) {
    $clamp(item, {clamp: 5, useNativeClamp: false});
});

//------------------------------------
function sortByPrice(param) {
    $('input[name = price_order]').val(param);
    var data = $('#bedding_filter_form').serializeArray();
    // console.log(data);
    // отправка Ajax
}



//----------------------------------------------
function showSelectedProduct(obj) {
    var oldprice = $('#select_size_product option:selected').attr('data-oldprice');
    var newprice = $('#select_size_product option:selected').attr('data-newprice');

    $('#show_select .old_price').html(oldprice);
    $('#show_select .new_price').html(newprice);

    // console.log(obj.value);
    // отправка Ajax для получения подробных данных о размере и комплектации
    // выбранного товара с ID = obj.value
}



//--------------------------------------------------------------------------------------
function changeItemTotal(obj) {
    var id = obj.name;
    var qnt = obj.value;
    // console.log(id);
    // console.log(qnt);
    // отправка Ajax для получения нового списка товаров с суммами и т.д.
}

//-----------------------------------------------------------------------------------------
function changeItemSize(id_old, id_new) {
    // отправка Ajax для получения нового списка товаров с суммами и т.д.
}


// -----------------------------------------------------------------------------
function removeItem(id) {
    // отправка Ajax для удаления товара и получения нового списка товаров с суммами и т.д.
}
