<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Db' => $baseDir . '/components/db.php',
    'RouterPsr4' => $baseDir . '/components/RouterPsr4.php',
);
