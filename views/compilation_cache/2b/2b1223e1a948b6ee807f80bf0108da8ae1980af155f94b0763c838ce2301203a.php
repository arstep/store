<?php

/* /include/site_left_sidebar_main.html.twig */
class __TwigTemplate_d74e75add2d2d836ee22d5427f819bde77440cc654a81362f760bc1aad4b5ba7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--sidebar-->
<ul id=\"leftsidebar\" class=\"col-sm-2 hidden-xs\">

    ";
        // line 5
        echo "    ";
        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "categories", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 8
            echo "        <li>
            <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "link", array(), "array"), "html", null, true);
            echo "\"><h4 class=\"heading\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "name", array(), "array"), "html", null, true);
            echo "</h4></a>
            <h5>По размеру</h5>
            <ul>
                ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "sizes", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["size"]) {
                // line 13
                echo "                    <li><a href=\"#\">1.5-спальное</a></li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['size'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "            </ul>

        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->";
    }

    public function getTemplateName()
    {
        return "/include/site_left_sidebar_main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 19,  55 => 15,  48 => 13,  44 => 12,  36 => 9,  33 => 8,  29 => 7,  26 => 6,  24 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--sidebar-->
<ul id=\"leftsidebar\" class=\"col-sm-2 hidden-xs\">

    {#{{ data['title'] }}#}
    {#{{ dump() }}#}

    {% for item in data['categories'] %}
        <li>
            <a href=\"{{ item['link'] }}\"><h4 class=\"heading\">{{ item['name'] }}</h4></a>
            <h5>По размеру</h5>
            <ul>
                {% for size in item['sizes'] %}
                    <li><a href=\"#\">1.5-спальное</a></li>
                {% endfor %}
            </ul>

        </li>
    {% endfor %}

    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->", "/include/site_left_sidebar_main.html.twig", "D:\\USR\\www\\store\\views\\include\\site_left_sidebar_main.html.twig");
    }
}
