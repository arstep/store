<?php

/* /templates/site_left_sidebar_main.html.twig */
class __TwigTemplate_eae69490afb35926edcb767df2b2af55babc1fc54eae04f3c96a455bf3f2f524 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--sidebar-->
<ul id=\"leftsidebar\" class=\"col-sm-2 hidden-xs\">

    ";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "title", array(), "array"), "html", null, true);
        echo "
    ";
        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, max(1, 3, 2), "html", null, true);
        echo "

    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->";
    }

    public function getTemplateName()
    {
        return "/templates/site_left_sidebar_main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 8,  30 => 7,  28 => 6,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--sidebar-->
<ul id=\"leftsidebar\" class=\"col-sm-2 hidden-xs\">

    {{ data['title'] }}
    {#{{ dump(data['title']) }}#}
    {#{{ dump(data) }}#}
    {#{{ dump() }}#}
    {{ max(1, 3, 2) }}

    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->", "/templates/site_left_sidebar_main.html.twig", "D:\\USR\\www\\store\\views\\templates\\site_left_sidebar_main.html.twig");
    }
}
