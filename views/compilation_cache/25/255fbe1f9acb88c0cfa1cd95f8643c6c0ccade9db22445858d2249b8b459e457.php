<?php

/* include/site_left_sidebar_category.html.twig */
class __TwigTemplate_1a2271e5c5893c2de0f6f14921da2043d0ee92f4decd293eed3be902ce759512 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    <!--filter-->
    <div id=\"bedding_filter\">
        <div>
            <a class=\"splLink btn btn-default heading visible-xs\" href=\"\">
                <i class=\"fa fa-sliders\" aria-hidden=\"true\"></i>&nbsp;&nbsp;
                Используйте фильтр параметров</a>
            <div class=\"splCont row\">
                <form id=\"bedding_filter_form\" action=\"#\" method=\"post\" class=\"<!--formstyle-->\">
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Размер</h4>
                        <ul>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_default\"
                                       value=\"default\" checked>
                                <label for=\"size_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_half\"
                                       value=\"half\">
                                <label for=\"size_half\">Полуторный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_double\"
                                       value=\"double\">
                                <label for=\"size_double\">Двухспальный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_euro\"
                                       value=\"euro\">
                                <label for=\"size_euro\">Евро</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_family\"
                                       value=\"family\">
                                <label for=\"size_family\">Семейный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_child\"
                                       value=\"child\">
                                <label for=\"size_child\">Детский</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Материал</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox checkbox_default\"
                                       id=\"material_default\" value=\"default\" checked/>
                                <label for=\"material_default\">Любой</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_sateen\" value=\"sateen\"/>
                                <label for=\"material_sateen\">Сатин</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_calico\" value=\"calico\"/>
                                <label for=\"material_calico\">Хлопок(Бязь)</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_jacquard\" value=\"jacquard\"/>
                                <label for=\"material_jacquard\">Жаккард</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_poplin\" value=\"poplin\"/>
                                <label for=\"material_poplin\">Поплин</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_silk\" value=\"silk\"/>
                                <label for=\"material_silk\">Шелк</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_viscose\" value=\"viscose\"/>
                                <label for=\"material_viscose\">Вискоза</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Наволочки в комплекте</h4>
                        <ul>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox checkbox_default\"
                                       id=\"pillowcase_default\" value=\"\" checked/>
                                <label for=\"pillowcase_default\">Любые</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_50x70\" value=\"\"/>
                                <label for=\"pillowcase_50x70\">50x70</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_70x70\" value=\"\"/>
                                <label for=\"pillowcase_70x70\">70x70</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_50x80\" value=\"\"/>
                                <label for=\"pillowcase_50x80\">50x80</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_80x80\" value=\"\"/>
                                <label for=\"pillowcase_80x80\">80x80</label></li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Стоимость</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox checkbox_default\" id=\"price_default\"
                                       value=\"\" checked/><label for=\"price_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to1500\"
                                       value=\"\"/><label for=\"price_to1500\">до 2000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to3000\"
                                       value=\"\"/><label for=\"price_to3000\">от 2001 до 4000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to4500\"
                                       value=\"\"/><label for=\"price_to4500\">от 4001 до 7000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_more7000\"
                                       value=\"\"/><label for=\"price_more7000\">выше 7001 &#8381;</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"clearfix\"></div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Бренд</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox checkbox_default\" id=\"brand_default\"
                                       value=\"\" checked/><label for=\"brand_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                        </ul>
                    </div>

                    <div class=\"clearfix\"></div>

                    <input type=\"hidden\" name=\"price_order\" value=\"asc\">
                    <input type=\"hidden\" name=\"product_count\" value=\"12\">
                    <input type=\"submit\" class=\"visible-xs\" value=\"Применить фильтр\">
                </form>
            </div>
        </div>
    </div>
    <!--end filter-->";
    }

    public function getTemplateName()
    {
        return "include/site_left_sidebar_category.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("    <!--filter-->
    <div id=\"bedding_filter\">
        <div>
            <a class=\"splLink btn btn-default heading visible-xs\" href=\"\">
                <i class=\"fa fa-sliders\" aria-hidden=\"true\"></i>&nbsp;&nbsp;
                Используйте фильтр параметров</a>
            <div class=\"splCont row\">
                <form id=\"bedding_filter_form\" action=\"#\" method=\"post\" class=\"<!--formstyle-->\">
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Размер</h4>
                        <ul>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_default\"
                                       value=\"default\" checked>
                                <label for=\"size_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_half\"
                                       value=\"half\">
                                <label for=\"size_half\">Полуторный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_double\"
                                       value=\"double\">
                                <label for=\"size_double\">Двухспальный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_euro\"
                                       value=\"euro\">
                                <label for=\"size_euro\">Евро</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_family\"
                                       value=\"family\">
                                <label for=\"size_family\">Семейный</label>
                            </li>
                            <li>
                                <input type=\"radio\" name=\"size\" class=\"radio\" id=\"size_child\"
                                       value=\"child\">
                                <label for=\"size_child\">Детский</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Материал</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox checkbox_default\"
                                       id=\"material_default\" value=\"default\" checked/>
                                <label for=\"material_default\">Любой</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_sateen\" value=\"sateen\"/>
                                <label for=\"material_sateen\">Сатин</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_calico\" value=\"calico\"/>
                                <label for=\"material_calico\">Хлопок(Бязь)</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_jacquard\" value=\"jacquard\"/>
                                <label for=\"material_jacquard\">Жаккард</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_poplin\" value=\"poplin\"/>
                                <label for=\"material_poplin\">Поплин</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_silk\" value=\"silk\"/>
                                <label for=\"material_silk\">Шелк</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"material\" class=\"checkbox\"
                                       id=\"material_viscose\" value=\"viscose\"/>
                                <label for=\"material_viscose\">Вискоза</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Наволочки в комплекте</h4>
                        <ul>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox checkbox_default\"
                                       id=\"pillowcase_default\" value=\"\" checked/>
                                <label for=\"pillowcase_default\">Любые</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_50x70\" value=\"\"/>
                                <label for=\"pillowcase_50x70\">50x70</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_70x70\" value=\"\"/>
                                <label for=\"pillowcase_70x70\">70x70</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_50x80\" value=\"\"/>
                                <label for=\"pillowcase_50x80\">50x80</label></li>
                            <li><input type=\"checkbox\" name=\"pillowcase\" class=\"checkbox\"
                                       id=\"pillowcase_80x80\" value=\"\"/>
                                <label for=\"pillowcase_80x80\">80x80</label></li>
                        </ul>
                    </div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Стоимость</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox checkbox_default\" id=\"price_default\"
                                       value=\"\" checked/><label for=\"price_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to1500\"
                                       value=\"\"/><label for=\"price_to1500\">до 2000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to3000\"
                                       value=\"\"/><label for=\"price_to3000\">от 2001 до 4000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_to4500\"
                                       value=\"\"/><label for=\"price_to4500\">от 4001 до 7000
                                    &#8381;</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"price\" class=\"checkbox\" id=\"price_more7000\"
                                       value=\"\"/><label for=\"price_more7000\">выше 7001 &#8381;</label>
                            </li>
                        </ul>
                    </div>
                    <div class=\"clearfix\"></div>
                    <div class=\"filter_goods col-sm-12 col-xs-6\">
                        <h4>Бренд</h4>
                        <ul>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox checkbox_default\" id=\"brand_default\"
                                       value=\"\" checked/><label for=\"brand_default\">Все</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\" id=\"brand_tango\"
                                       value=\"\"/><label for=\"brand_tango\">TANGO</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                            <li>
                                <input type=\"checkbox\" name=\"brand\" class=\"checkbox\"
                                       id=\"brand_incalpaca\"
                                       value=\"\"/><label for=\"brand_incalpaca\">INCALPACA</label>
                            </li>
                        </ul>
                    </div>

                    <div class=\"clearfix\"></div>

                    <input type=\"hidden\" name=\"price_order\" value=\"asc\">
                    <input type=\"hidden\" name=\"product_count\" value=\"12\">
                    <input type=\"submit\" class=\"visible-xs\" value=\"Применить фильтр\">
                </form>
            </div>
        </div>
    </div>
    <!--end filter-->", "include/site_left_sidebar_category.html.twig", "D:\\USR\\www\\store\\views\\include\\site_left_sidebar_category.html.twig");
    }
}
