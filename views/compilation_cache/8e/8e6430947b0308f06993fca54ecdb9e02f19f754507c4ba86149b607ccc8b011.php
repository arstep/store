<?php

/* include/product_block.html.twig */
class __TwigTemplate_c7bf115bcedaa3929339a6f7df639e364b769cba0a6b2542a304762928eb3994 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<h1 class=\"heading\">";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "name", array()), "html", null, true);
        echo "</h1>

<div id=\"show_select\">

    ";
        // line 7
        echo "    ";
        if ((null === twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "price_old", array()))) {
            // line 8
            echo "        <span>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "price", array()), "html", null, true);
            echo "&#8381;</span>
    ";
        } else {
            // line 10
            echo "        <span class=\"old_price\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "price_old", array()), "html", null, true);
            echo "</span>
        <span class=\"new_price\">";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "price", array()), "html", null, true);
            echo "&#8381;</span>
    ";
        }
        // line 13
        echo "
</div>

<div id=\"product_data\">
    <form action=\"\" method=\"post\">
        <select class=\"form-control\" id=\"select_size_product\" onchange=\"showSelectedProduct(this)\"
                name=\"id\">

            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "prices", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
            // line 22
            echo "                <option data-oldprice=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price_old", array()), "html", null, true);
            echo "\" data-newprice=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price", array()), "html", null, true);
            echo "&#8381;\"
                        value=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "id", array()), "html", null, true);
            echo "\"
                        ";
            // line 24
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price", array()) == twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "price", array()))) {
                // line 25
                echo "                            selected
                        ";
            }
            // line 27
            echo "                >";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "size_title", array())), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price", array()), "html", null, true);
            echo "&#8381;

                </option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "
        </select>
        <br>
        <input type=\"submit\" class=\"btn btn-default\" value=\"В корзину\">
    </form>
</div>

<div id=\"product_info\">
    <h5>Основная информация</h5>
    <ul>
        <li><strong>Материал:</strong>&nbsp;";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "material_description", array()), "html", null, true);
        echo "</li>
        <li><strong>Плотность:</strong>&nbsp;";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "density", array()), "html", null, true);
        echo " г/м2</li>
        <li><strong>Размерность:</strong>&nbsp;";
        // line 43
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "size_title", array())), "html", null, true);
        echo "</li>
        <li><strong>Комплект:</strong>&nbsp;наволочки ";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "pillowcases_title", array()), "html", null, true);
        echo ",
            ";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "size_description", array()), "html", null, true);
        echo "</li>
    </ul>
    <h5>Дополнительно</h5>
    <ul>
        ";
        // line 50
        echo "        ";
        // line 51
        echo "        <li><strong>Бренд:</strong>&nbsp;";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "brand_title", array()), "html", null, true);
        echo "</li>
        <li><strong>Страна происхождения:</strong>&nbsp;";
        // line 52
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "region_title", array())), "html", null, true);
        echo "</li>
    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "include/product_block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 52,  129 => 51,  127 => 50,  120 => 45,  116 => 44,  112 => 43,  108 => 42,  104 => 41,  92 => 31,  79 => 27,  75 => 25,  73 => 24,  69 => 23,  62 => 22,  58 => 21,  48 => 13,  43 => 11,  38 => 10,  32 => 8,  29 => 7,  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<h1 class=\"heading\">{{ data.product.name }}</h1>

<div id=\"show_select\">

    {#Если нет старой цены - отображаем без зачеркиваний и красного цвета. В БД должен быть NULL !!!#}
    {% if data.product.price_old is null %}
        <span>{{ data.product.price }}&#8381;</span>
    {% else %}
        <span class=\"old_price\">{{ data.product.price_old }}</span>
        <span class=\"new_price\">{{ data.product.price }}&#8381;</span>
    {% endif %}

</div>

<div id=\"product_data\">
    <form action=\"\" method=\"post\">
        <select class=\"form-control\" id=\"select_size_product\" onchange=\"showSelectedProduct(this)\"
                name=\"id\">

            {% for value in data.product.prices %}
                <option data-oldprice=\"{{ value.price_old }}\" data-newprice=\"{{ value.price }}&#8381;\"
                        value=\"{{ value.id }}\"
                        {% if value.price == data.product.price %}
                            selected
                        {% endif %}
                >{{ value.size_title | capitalize }} - {{ value.price }}&#8381;

                </option>
            {% endfor %}

        </select>
        <br>
        <input type=\"submit\" class=\"btn btn-default\" value=\"В корзину\">
    </form>
</div>

<div id=\"product_info\">
    <h5>Основная информация</h5>
    <ul>
        <li><strong>Материал:</strong>&nbsp;{{ data.product.material_description }}</li>
        <li><strong>Плотность:</strong>&nbsp;{{ data.product.density }} г/м2</li>
        <li><strong>Размерность:</strong>&nbsp;{{ data.product.size_title | capitalize }}</li>
        <li><strong>Комплект:</strong>&nbsp;наволочки {{ data.product.pillowcases_title }},
            {{ data.product.size_description }}</li>
    </ul>
    <h5>Дополнительно</h5>
    <ul>
        {#<li><strong>Застежка наволочек:</strong> Запах (клапан)</li>#}
        {#<li><strong>Застежка пододеяльника:</strong> Молния/пуговицы - в зависимости от партии</li>#}
        <li><strong>Бренд:</strong>&nbsp;{{ data.product.brand_title }}</li>
        <li><strong>Страна происхождения:</strong>&nbsp;{{ data.product.region_title | capitalize }}</li>
    </ul>
</div>", "include/product_block.html.twig", "D:\\USR\\www\\store\\views\\include\\product_block.html.twig");
    }
}
