<?php

/* site/index.html.twig */
class __TwigTemplate_23862f2938feb18474644b5b65750c6741c87125e543661ff173cfc6b8a908a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("/templates/site_layout.html.twig", "site/index.html.twig", 1);
        $this->blocks = array(
            'buttonCart' => array($this, 'block_buttonCart'),
            'bredCrumbs' => array($this, 'block_bredCrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/templates/site_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_buttonCart($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("buttonCart", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_bredCrumbs($context, array $blocks = array())
    {
        // line 8
        echo "    ";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "
    ";
        // line 13
        $this->loadTemplate("/templates/site_left_sidebar_main.html.twig", "site/index.html.twig", 13)->display($context);
        // line 14
        echo "
    <div class=\"col-sm-10 rightcontent\">

        <div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"150000\"
             style=\"max-width: 833px;\">
            <!-- Wrapper for slides -->
            <div class=\"carousel-inner\" role=\"listbox\">
                <div class=\"item active\">
                    <div class=\"item-responsive item-16by9\">
                        <div class=\"content\" style=\"background: url(image/bigImageGood3.jpg);\"></div>
                    </div>
                    <div class=\"carousel-caption\" onclick='location.href=\"/good.php\"'>
                        <div class=\"labelbox\">
                            <span class=\"carouseloldprice\">350 &#8381;</span><br>
                            <span class=\"carouselprice\">500 &#8381;</span>
                        </div>
                        <span class=\"carouseltitle\">Lorem ipsum dolor sit amet</span>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"item-responsive item-16by9\">
                        <div class=\"content\" style=\"background: url(image/bigImageGood1.jpg);\"></div>
                    </div>
                    <div class=\"carousel-caption\" onclick='location.href=\"/good.php\"'>
                        <div class=\"labelbox\">
                            <span class=\"carouseloldprice\">3500 &#8381;</span><br>
                            <span class=\"carouselprice\">5000 &#8381;</span>
                        </div>
                        <span class=\"carouseltitle\">Lorem ipsum dolor sit amet</span>
                    </div>
                </div>
                <div class=\"item\">
                    <div class=\"item-responsive item-16by9\">
                        <div class=\"content\" style=\"background: url(image/bigImageGood2.jpg);\"></div>
                    </div>
                    <div class=\"carousel-caption\" onclick='location.href=\"/good.php\"'>
                        <div class=\"labelbox\">
                            <span class=\"carouseloldprice\">35000 &#8381;</span><br>
                            <span class=\"carouselprice\">50000 &#8381;</span>
                        </div>
                        <span class=\"carouseltitle\">Lorem ipsum dolor sit amet</span>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class=\"left carousel-control\" href=\"#carousel\" role=\"button\" data-slide=\"prev\">
                <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"right carousel-control\" href=\"#carousel\" role=\"button\" data-slide=\"next\">
                <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">Next</span>
            </a>
        </div>

        <div class=\"clearfix\"></div>

        <!--categories-->
        <p class=\"heading\">Выбрать категорию</p>
        <ul class=\"row category\">

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"postelnoe_belye.html\"><img src=\"image/complect.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Постельное белье</h2>
                        </div>
                    </a>
                </div>
            </li>

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/polot.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Покрывала</h2>
                        </div>
                    </a>
                </div>
            </li>

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/plaid.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Пледы</h2>
                        </div>
                    </a>
                </div>
            </li>

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/comforter.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Одеяла</h2>
                        </div>
                    </a>
                </div>
            </li>

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/pillows.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Подушки</h2>
                        </div>
                    </a>
                </div>
            </li>

            <li class=\"col-sm-4 col-xs-6\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/curtains.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h2>Шторы</h2>
                        </div>
                    </a>
                </div>
            </li>
            <!--end og categories-->
        </ul>


        <p class=\"heading\">Производители</p>
        <ul id=\"brands\">
            <li class=\"brands\"><a href=\"#\">INCALPACA</a></li>
            <li class=\"brands\"><a href=\"#\">TANGO</a></li>
            <li class=\"brands\"><a href=\"#\">Kingsilk</a></li>
            <li class=\"brands\"><a href=\"#\">ТЕТ-А-ТЕТ</a></li>
            <li class=\"brands\"><a href=\"#\">VALTERY</a></li>
            <li class=\"brands\"><a href=\"#\">CLASSI</a></li>
            <li class=\"brands\"><a href=\"#\">ACACIA</a></li>
            <li class=\"brands\"><a href=\"#\">NEWTONE</a></li>
            <li class=\"brands\"><a href=\"#\">MODALIN</a></li>
            <li class=\"clearfix\"></li>
        </ul>


        <p class=\"heading\">СТАТЬИ, КОТОРЫЕ МОГУТ ВАС ЗАИНТЕРЕСОВАТЬ</p>
        <div class=\"row\">
            <div class=\"col-sm-4 presentarticles\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/turkish-cotton.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h4 class=\"clampestitle\">ОРГАНИЧЕСКИЙ ТУРЕЦКИЙ ХЛОПОК: ЭКОЛОГИЧНЫЙ И РОСКОШНЫЙ
                                ТУРЕЦКИЙ ХЛОПОК</h4>
                            <p class=\"clampestext\">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Alias architecto
                                assumenda autem cumque cupiditate doloremque eum eveniet explicabo id
                                inventore, ipsam</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-sm-4 presentarticles\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/bedding-fall-and-winter.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h4 class=\"clampestitle\">УЮТНАЯ ПОСТЕЛЬ ДЛЯ ОСЕНИ И ЗИМЫ</h4>
                            <p class=\"clampestext\">Не может быть ничего лучше, чем сжигать холодные месяцы
                                осени и зимы</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"col-sm-4 presentarticles\">
                <div class=\"thumbnail\">
                    <a href=\"#\"><img src=\"image/th_hudson-bedroom.jpg\" alt=\"...\">
                        <div class=\"caption\">
                            <h4 class=\"clampestitle\">КОЛЛЕКЦИЯ HUDSON BEDROOM</h4>
                            <p class=\"clampestext\">История позади управляемой плантации, где собрано красное
                                дерево для нашей
                                мебели для спальни Hudson Collection, и внимательный </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "site/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 14,  53 => 13,  50 => 12,  47 => 11,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/index.html.twig", "D:\\USR\\www\\store\\views\\site\\index.html.twig");
    }
}
