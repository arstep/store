<?php

/* include/catalog_block.html.twig */
class __TwigTemplate_391ab79f25132a8d691bdec1d036b458ac5a11076b8004b32b1c810ffae9ed66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 3
            echo "
    <li class=\"col-sm-4 col-xs-6\">

        <div class=\"item_container\">
            <a href=\"/catalog/";
            // line 7
            echo twig_escape_filter($this->env, ($context["category"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "id", array()), "html", null, true);
            echo "\">
                <div class=\"list_image\">
                    <img src=\"/";
            // line 9
            echo twig_escape_filter($this->env, ($context["pathPhotoGoods"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "photo", array()), 0, array()), "name", array()), "html", null, true);
            echo "\" alt=\"\">
                </div>
                <div>
                    <h3 class=\"list_heading clampestitle\">";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "name", array()), "html", null, true);
            echo "</h3>
                    <p class=\"list_heading list_hidden\">";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "name", array()), "html", null, true);
            echo "</p>
                    <p class=\"list_price\">

                        ";
            // line 17
            echo "                        ";
            if ((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price_old", array()))) {
                // line 18
                echo "                            <span>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price", array()), "html", null, true);
                echo "&#8381;</span>
                        ";
            } else {
                // line 20
                echo "                            ";
                // line 21
                echo "                            <span class=\"old_price\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price_old", array()), "html", null, true);
                echo "</span>
                            <span class=\"new_price\">";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "price", array()), "html", null, true);
                echo "&#8381;</span>
                        ";
            }
            // line 24
            echo "
                    </p>
                    <p class=\"list_size\">Размер: <span>";
            // line 26
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "size_title", array())), "html", null, true);
            echo "</span></p>
                    <div class=\"list_hidden\">
                        <hr>
                        <p class=\"list_material\">Материал: <span>";
            // line 29
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "material_title", array())), "html", null, true);
            echo "</span></p>
                        <p class=\"list_brand\">Бренд: <span>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "brand_title", array()), "html", null, true);
            echo "</span></p>
                        <p class=\"list_pillowcases\">Наволочки: <span>";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "pillowcases_title", array()), "html", null, true);
            echo "</span></p>
                        <ul class=\"desc_price\">

                            ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["value"], "prices", array()));
            foreach ($context['_seq'] as $context["num"] => $context["item"]) {
                // line 35
                echo "                                <li>
                                    <img src=\"/image/icon-";
                // line 36
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "size_name", array()), "html", null, true);
                echo ".png\" title=\"";
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "size_title", array())), "html", null, true);
                echo "\" alt=\"\">&nbsp;
                                    ";
                // line 38
                echo "                                    ";
                if ((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "price_old", array()))) {
                    // line 39
                    echo "                                        <span>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "price", array()), "html", null, true);
                    echo "&#8381;</span>
                                    ";
                } else {
                    // line 41
                    echo "                                        <span class=\"old_price\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "price_old", array()), "html", null, true);
                    echo "&#8381;</span>
                                        <span class=\"new_price\">";
                    // line 42
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "price", array()), "html", null, true);
                    echo "&#8381;</span>
                                    ";
                }
                // line 44
                echo "                                </li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['num'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "
                        </ul>
                    </div>
                </div>
            </a>
        </div>
    </li>

";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 55
            echo "    Нет данных
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "include/catalog_block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 55,  142 => 46,  135 => 44,  130 => 42,  125 => 41,  119 => 39,  116 => 38,  110 => 36,  107 => 35,  103 => 34,  97 => 31,  93 => 30,  89 => 29,  83 => 26,  79 => 24,  74 => 22,  69 => 21,  67 => 20,  61 => 18,  58 => 17,  52 => 13,  48 => 12,  40 => 9,  33 => 7,  27 => 3,  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% for key, value in collection %}

    <li class=\"col-sm-4 col-xs-6\">

        <div class=\"item_container\">
            <a href=\"/catalog/{{ category }}/{{ value.id }}\">
                <div class=\"list_image\">
                    <img src=\"/{{ pathPhotoGoods }}/{{ value.photo.0.name }}\" alt=\"\">
                </div>
                <div>
                    <h3 class=\"list_heading clampestitle\">{{ value.name }}</h3>
                    <p class=\"list_heading list_hidden\">{{ value.name }}</p>
                    <p class=\"list_price\">

                        {#Если нет старой цены - отображаем без зачеркиваний и красного цвета. В БД должен быть NULL !!!#}
                        {% if value.price_old is null %}
                            <span>{{ value.price }}&#8381;</span>
                        {% else %}
                            {#Если старая цена есть, то она будет отображена зачеркнутой, а основная (новая) - красной#}
                            <span class=\"old_price\">{{ value.price_old }}</span>
                            <span class=\"new_price\">{{ value.price }}&#8381;</span>
                        {% endif %}

                    </p>
                    <p class=\"list_size\">Размер: <span>{{ value.size_title | capitalize }}</span></p>
                    <div class=\"list_hidden\">
                        <hr>
                        <p class=\"list_material\">Материал: <span>{{ value.material_title | capitalize }}</span></p>
                        <p class=\"list_brand\">Бренд: <span>{{ value.brand_title }}</span></p>
                        <p class=\"list_pillowcases\">Наволочки: <span>{{ value.pillowcases_title }}</span></p>
                        <ul class=\"desc_price\">

                            {% for num,item in value.prices %}
                                <li>
                                    <img src=\"/image/icon-{{ item.size_name }}.png\" title=\"{{ item.size_title | capitalize }}\" alt=\"\">&nbsp;
                                    {#Если нет старой цены - отображаем без зачеркиваний и красного цвета. В БД должен быть NULL !!!#}
                                    {% if item.price_old is null %}
                                        <span>{{ item.price }}&#8381;</span>
                                    {% else %}
                                        <span class=\"old_price\">{{ item.price_old }}&#8381;</span>
                                        <span class=\"new_price\">{{ item.price }}&#8381;</span>
                                    {% endif %}
                                </li>
                            {% endfor %}

                        </ul>
                    </div>
                </div>
            </a>
        </div>
    </li>

{% else %}
    Нет данных
{% endfor %}", "include/catalog_block.html.twig", "D:\\USR\\www\\store\\views\\include\\catalog_block.html.twig");
    }
}
