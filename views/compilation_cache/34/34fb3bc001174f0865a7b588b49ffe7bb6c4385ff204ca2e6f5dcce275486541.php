<?php

/* /templates/site_layout.html.twig */
class __TwigTemplate_fa37525e19661523713f352f10832ee250a31fc7d155b6cd3b37b5fee5d860e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'buttonCart' => array($this, 'block_buttonCart'),
            'mainBanner' => array($this, 'block_mainBanner'),
            'bredCrumbs' => array($this, 'block_bredCrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <title>Главная</title>
    <link rel=\"stylesheet\" href=\"css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"css/font-awesome.css\">
    <link rel=\"stylesheet\" href=\"css/style.css\">
    <link rel=\"stylesheet\" href=\"css/flexslider.css\" type=\"text/css\" media=\"screen\"/>
    <link rel=\"stylesheet\" href=\"css/lightbox.css\">


    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <!--<meta http-equiv=\"cleartype\" content=\"on\">-->
    <meta name=\"MobileOptimized\" content=\"320\">
    <meta name=\"HandheldFriendly\" content=\"True\">
    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">

    <link href=\"https://fonts.googleapis.com/css?family=Fascinate\" rel=\"stylesheet\">
    <!--font-family: 'Fascinate', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Poller+One\" rel=\"stylesheet\">
    <!--font-family: 'Poller One', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Titan+One\" rel=\"stylesheet\">
    <!--font-family: 'Titan One', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Yeseva+One\" rel=\"stylesheet\">
    <!--font-family: 'Yeseva One', cursive;-->

    <link href=\"https://fonts.googleapis.com/css?family=Raleway\" rel=\"stylesheet\">
    <!--font-family: 'Raleway', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=PT+Sans\" rel=\"stylesheet\">
    <!--font-family: 'PT Sans', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Ubuntu\" rel=\"stylesheet\">
    <!--font-family: 'Ubuntu', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\">
    <!--font-family: 'Lato', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Poiret+One\" rel=\"stylesheet\">
    <!--font-family: 'Poiret One', cursive;-->
</head>
<body>


<main id=\"panel\" class=\"panel slidecontainer\">

    <header class=\"panel-header\">
        <button class=\"btn-hamburger js-slideout-toggle\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></button>
    </header>

    <section class=\"box\">

        <div id=\"topstring\">
            <div class=\"container\">
                <div class=\"row\">
                    <div id=\"toplink\"><a href=\"#\">webcotton.ru</a></div>
                    <div id=\"topphone\">Москва: 8 195 555-25-25</div>
                    <div id=\"toptext\" class=\"hidden-xs\">Заказывайте на сайте в любое время! Обработаем заказ с 10:00 до
                        20:00,
                        без выходных.
                    </div>
                </div>
            </div>
        </div>

        <div class=\"container\">

            <div class=\"row\">
                <div id=\"logotype\" class=\"col-sm-8\" style=\"font-family: 'Fascinate', cursive\">
                    <a href=\"index.html\">WebCotton</a>
                    <h1>Интернет-магазин текстиля для дома</h1>
                </div>

                <div id=\"cart\" class=\"col-sm-4\">

                    ";
        // line 74
        $this->displayBlock('buttonCart', $context, $blocks);
        // line 131
        echo "
                </div>
            </div>


            <div class=\"row\">
                <!-- NavPanel -->
                <nav id=\"nav\">
                    <ul>
                        <li class=\"nav-top-title\"><p>Каталог</p>
                            <ul class=\"nav-top-section\">

                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/complect.jpg\" alt=\"\"><br>
                                        Постельное белье</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                        <li><a href=\"#\">Супер Евро</a></li>
                                        <li>
                                            <hr>
                                        </li>
                                        <li><a href=\"#\">Простыни</a></li>
                                        <li><a href=\"#\">Наволочки</a></li>
                                        <li><a href=\"#\">Пододеяльники</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/polot.jpg\" alt=\"\"><br>
                                        Покрывала</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/plaid.jpg\" alt=\"\"><br>
                                        Пледы</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/comforter.jpg\" alt=\"\"><br>
                                        Одеяла</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/pillows.jpg\" alt=\"\"><br>
                                        Подушки</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/curtains.jpg\" alt=\"\"><br>
                                        Шторы</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li class=\"nav-top-title\"><a href=\"/\">Доставка</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Оплата</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Статьи</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Правила и условия</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Контакты</a></li>
                    </ul>
                </nav>
                <!-- end NavPanel -->
            </div>

            ";
        // line 223
        $this->displayBlock('mainBanner', $context, $blocks);
        // line 228
        echo "
            ";
        // line 229
        $this->displayBlock('bredCrumbs', $context, $blocks);
        // line 246
        echo "

            <div class=\"row maincontent\">

                ";
        // line 250
        $this->displayBlock('content', $context, $blocks);
        // line 365
        echo "
            </div>
        </div>

        <footer>
            <div class=\"container\">
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Каталог</p>
                    <ul>
                        <li><a href=\"#\">Постельное белье</a></li>
                        <li><a href=\"#\">Покрывала</a></li>
                        <li><a href=\"#\">Пледы</a></li>
                        <li><a href=\"#\">Одеяла</a></li>
                        <li><a href=\"#\">Подушки</a></li>
                        <li><a href=\"#\">Шторы</a></li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Информация</p>
                    <ul>
                        <li><a href=\"#\">Гарантия</a></li>
                        <li><a href=\"#\">Доставка</a></li>
                        <li><a href=\"#\">Карта сайта</a></li>
                        <li><a href=\"#\">Полезная информация</a></li>
                        <li><a href=\"#\">Политика конфиденциальности</a></li>
                        <li><a href=\"#\">Правила и условия</a></li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Контакты</p>
                    <ul>
                        <li>2016 &copy; Магазин текстиля WebCotton.ru</li>
                        <li>E-mail: info@webcotton.ru</li>
                        <li><abbr title=\"Телефон\">тел.:</abbr> 8 195 555-25-25</li>
                        <li>Режим работы: Без выходных с 10:00 до 20:00.</li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Обратная связь</p>
                    <form role=\"form\">
                        <div class=\"form-group\">
                            <label for=\"InputEmail\">Ваш Email</label>
                            <input type=\"email\" class=\"form-control\" id=\"InputEmail\" placeholder=\"Введите email\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"InputText\">Ваше сообщение</label>
                            <textarea id=\"InputText\" class=\"form-control\" rows=\"3\"></textarea>
                        </div>
                        <button type=\"submit\" class=\"btn btn-default\">Отправить</button>
                    </form>
                </div>
            </div>
        </footer>
    </section>
</main>


<a id=\"toTop\" class=\"btn btn-default\" href=\"/\" role=\"button\">
    <i class=\"glyphicon glyphicon-chevron-up\" aria-hidden=\"true\"></i> Наверх
</a>


<!--Slideout-->
<nav id=\"menu\" class=\"menu\">
    <header class=\"menu-header\">
        <a href=\"#\" class=\"menu-header-title\">WebCotton</a>
    </header>
    <ul class=\"menu-section\">
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Каталог</a>
            <ul class=\"menu-section-first\">
                <li><a href=\"#\">Постельное белье</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                        <li><a href=\"#\">Супер Евро</a></li>
                        <li>
                            <hr>
                        </li>
                        <li><a href=\"#\">Простыни</a></li>
                        <li><a href=\"#\">Наволочки</a></li>
                        <li><a href=\"#\">Пододеяльники</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Покрывала</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Пледы</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Одеяла</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Подушки</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Шторы</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Доставка</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Оплата</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Статьи</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Правила и условия</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Контакты</a></li>
    </ul>
</nav>
<!--Slideout-->

<!-- Scripts -->
<!--<script src=\"js/jquery-3.2.1.min.js\"></script>-->
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js\"></script>
<script src=\"js/bootstrap.min.js\"></script>
<script src=\"js/to_top.js\"></script>
<script src=\"js/slideout.js\"></script>
<script src=\"js/clamp.js\"></script>

<script src=\"js/functions.js\"></script>


<script src=\"js/lightbox.js\"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true
    })
</script>

<!-- FlexSlider -->
<script defer src=\"js/jquery.flexslider.js\"></script>

<script type=\"text/javascript\">
    \$(window).load(function () {
        \$('#carousel').flexslider({
            animation: \"slide\",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 130,
            itemMargin: 5,
            asNavFor: '#slider'
        });

        \$('#slider').flexslider({
            animation: \"slide\",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: \"#carousel\",
            start: function (slider) {
                \$('body').removeClass('loading');
            }
        });
    });
</script>

</body>
</html>";
    }

    // line 74
    public function block_buttonCart($context, array $blocks = array())
    {
        // line 75
        echo "                    <a href=\"javascript:void(0)\">КОРЗИНА (6)</a>
                    <div id=\"cartinner\">
                        <div><p>Товаров в корзине: 6</p></div>
                        <div id=\"cartlistgoods\">
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">
                                    <p class=\"cartnewprice\">3500&#8381;</p>
                                    <p class=\"cartoldprice\">4300&#8381;</p>
                                </div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">1800&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">
                                    <p class=\"cartnewprice\">2300&#8381;</p>
                                    <p class=\"cartoldprice\">2700&#8381;</p>
                                </div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                        </div>
                        <div class=\"itemincart subtotal row\">
                            <div>Итог: 10500 &#8381;</div>
                        </div>
                        <div id=\"cartfooter\">
                            <a href=\"shopping_cart.html\">СМОТРЕТЬ</a>
                            <a href=\"#\">ОФОРМИТЬ</a>
                        </div>
                    </div>
                    ";
    }

    // line 223
    public function block_mainBanner($context, array $blocks = array())
    {
        // line 224
        echo "                <div class=\"row\" id=\"mainBanner\">
                    <a href=\"#\">Скидка <span>20%</span> на постельные комплекты VALTERY</a>
                </div>
            ";
    }

    // line 229
    public function block_bredCrumbs($context, array $blocks = array())
    {
        // line 230
        echo "                <div class=\"row\">
                    <ul class=\"crumbs\">
                        <li>
                            <a href=\"index.html\">WebCotton</a>
                            <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
                        </li>
                        <li>
                            <a href=\"index.html\">Корзина покупателя</a>
                            <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
                        </li>
                        <li class=\"active\">
                            <span>Оформление заказа</span>
                        </li>
                    </ul>
                </div>
            ";
    }

    // line 250
    public function block_content($context, array $blocks = array())
    {
        // line 251
        echo "                <div id=\"order\" class=\"col-sm-7 col-xs-12\">
                    <p class=\"heading\">Оформление заказа</p>
                    <form action=\"#\" method=\"post\" role=\"form\">
                        <div class=\"form-group\">
                            <label for=\"order_name\">Фамилия, Имя:*</label>
                            <input type=\"text\" name=\"order_name\" class=\"form-control\" id=\"order_name\"
                                   placeholder=\"Иванов Иван\" required>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_phone\">Телефон:*</label>
                            <input type=\"tel\" name=\"order_phone\" class=\"form-control\" id=\"order_phone\"
                                   placeholder=\"8-918-123-4567, 8-495-123-4567\" required>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_email\">Email:</label>
                            <input type=\"email\" name=\"order_email\" class=\"form-control\" id=\"order_email\"
                                   placeholder=\"adress@domain.ru\">
                        </div>

                        <p class=\"heading\">Доставка*</p>
                        <div class=\"form-group\">
                            <label for=\"order_city\">Населенный пункт:</label>
                            <input type=\"text\" name=\"order_city\" class=\"form-control\" id=\"order_city\"
                                   placeholder=\"Укажите город\"
                                   value=\"Москва\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_adress\">Ваш адрес:</label>
                            <input type=\"text\" name=\"order_adress\" class=\"form-control\" id=\"order_adress\"
                                   placeholder=\"ул. Тверская, д. 1, кв. 1\">
                        </div>
                        <div>
                            <label for=\"order_text\">Примечание:</label>
                            <textarea class=\"form-control\" name=\"order_text\" id=\"order_text\" rows=\"3\"
                                      placeholder=\"Домофон, подъезд, дата, время доставки\"></textarea>
                        </div>

                        <p class=\"small\">* отмечены обязательные пункты</p>

                        <input type=\"submit\" class=\"cartsubmit\" href=\"#\" value=\"ОТПРАВИТЬ\">
                    </form>
                </div>

                <div id=\"orderlist\" class=\"col-sm-5 hidden-xs\">
                    <p class=\"heading\">Заказ</p>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <div class=\"col-sm-7\">
                                <p>Итог заказа:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p id=\"orderlist_total\">5200 &#8381;</p>
                            </div>
                        </div>
                    </div>
                </div>
                ";
    }

    public function getTemplateName()
    {
        return "/templates/site_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  495 => 251,  492 => 250,  473 => 230,  470 => 229,  463 => 224,  460 => 223,  401 => 75,  398 => 74,  209 => 365,  207 => 250,  201 => 246,  199 => 229,  196 => 228,  194 => 223,  100 => 131,  98 => 74,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <title>Главная</title>
    <link rel=\"stylesheet\" href=\"css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"css/font-awesome.css\">
    <link rel=\"stylesheet\" href=\"css/style.css\">
    <link rel=\"stylesheet\" href=\"css/flexslider.css\" type=\"text/css\" media=\"screen\"/>
    <link rel=\"stylesheet\" href=\"css/lightbox.css\">


    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <!--<meta http-equiv=\"cleartype\" content=\"on\">-->
    <meta name=\"MobileOptimized\" content=\"320\">
    <meta name=\"HandheldFriendly\" content=\"True\">
    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">

    <link href=\"https://fonts.googleapis.com/css?family=Fascinate\" rel=\"stylesheet\">
    <!--font-family: 'Fascinate', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Poller+One\" rel=\"stylesheet\">
    <!--font-family: 'Poller One', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Titan+One\" rel=\"stylesheet\">
    <!--font-family: 'Titan One', cursive;-->
    <link href=\"https://fonts.googleapis.com/css?family=Yeseva+One\" rel=\"stylesheet\">
    <!--font-family: 'Yeseva One', cursive;-->

    <link href=\"https://fonts.googleapis.com/css?family=Raleway\" rel=\"stylesheet\">
    <!--font-family: 'Raleway', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=PT+Sans\" rel=\"stylesheet\">
    <!--font-family: 'PT Sans', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Ubuntu\" rel=\"stylesheet\">
    <!--font-family: 'Ubuntu', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\">
    <!--font-family: 'Lato', sans-serif;-->
    <link href=\"https://fonts.googleapis.com/css?family=Poiret+One\" rel=\"stylesheet\">
    <!--font-family: 'Poiret One', cursive;-->
</head>
<body>


<main id=\"panel\" class=\"panel slidecontainer\">

    <header class=\"panel-header\">
        <button class=\"btn-hamburger js-slideout-toggle\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></button>
    </header>

    <section class=\"box\">

        <div id=\"topstring\">
            <div class=\"container\">
                <div class=\"row\">
                    <div id=\"toplink\"><a href=\"#\">webcotton.ru</a></div>
                    <div id=\"topphone\">Москва: 8 195 555-25-25</div>
                    <div id=\"toptext\" class=\"hidden-xs\">Заказывайте на сайте в любое время! Обработаем заказ с 10:00 до
                        20:00,
                        без выходных.
                    </div>
                </div>
            </div>
        </div>

        <div class=\"container\">

            <div class=\"row\">
                <div id=\"logotype\" class=\"col-sm-8\" style=\"font-family: 'Fascinate', cursive\">
                    <a href=\"index.html\">WebCotton</a>
                    <h1>Интернет-магазин текстиля для дома</h1>
                </div>

                <div id=\"cart\" class=\"col-sm-4\">

                    {% block buttonCart %}
                    <a href=\"javascript:void(0)\">КОРЗИНА (6)</a>
                    <div id=\"cartinner\">
                        <div><p>Товаров в корзине: 6</p></div>
                        <div id=\"cartlistgoods\">
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">
                                    <p class=\"cartnewprice\">3500&#8381;</p>
                                    <p class=\"cartoldprice\">4300&#8381;</p>
                                </div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">1800&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">
                                    <p class=\"cartnewprice\">2300&#8381;</p>
                                    <p class=\"cartoldprice\">2700&#8381;</p>
                                </div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                            <div class=\"itemincart row\">
                                <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
                                <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                                    <p class=\"cartpricegood\">Количество: 1</p></div>
                                <div class=\"col-xs-3\">3500&#8381;</div>
                            </div>
                        </div>
                        <div class=\"itemincart subtotal row\">
                            <div>Итог: 10500 &#8381;</div>
                        </div>
                        <div id=\"cartfooter\">
                            <a href=\"shopping_cart.html\">СМОТРЕТЬ</a>
                            <a href=\"#\">ОФОРМИТЬ</a>
                        </div>
                    </div>
                    {% endblock %}

                </div>
            </div>


            <div class=\"row\">
                <!-- NavPanel -->
                <nav id=\"nav\">
                    <ul>
                        <li class=\"nav-top-title\"><p>Каталог</p>
                            <ul class=\"nav-top-section\">

                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/complect.jpg\" alt=\"\"><br>
                                        Постельное белье</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                        <li><a href=\"#\">Супер Евро</a></li>
                                        <li>
                                            <hr>
                                        </li>
                                        <li><a href=\"#\">Простыни</a></li>
                                        <li><a href=\"#\">Наволочки</a></li>
                                        <li><a href=\"#\">Пододеяльники</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/polot.jpg\" alt=\"\"><br>
                                        Покрывала</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/plaid.jpg\" alt=\"\"><br>
                                        Пледы</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/comforter.jpg\" alt=\"\"><br>
                                        Одеяла</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/pillows.jpg\" alt=\"\"><br>
                                        Подушки</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>
                                <li class=\"nav-second-title\">
                                    <a href=\"#\"><img src=\"image/curtains.jpg\" alt=\"\"><br>
                                        Шторы</a>
                                    <ul class=\"nav-second-section\">
                                        <li><a href=\"#\">1.5-спальное</a></li>
                                        <li><a href=\"#\">2-спальное</a></li>
                                        <li><a href=\"#\">Семейное</a></li>
                                        <li><a href=\"#\">Евро</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li class=\"nav-top-title\"><a href=\"/\">Доставка</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Оплата</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Статьи</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Правила и условия</a></li>
                        <li class=\"nav-top-title\"><a href=\"/\">Контакты</a></li>
                    </ul>
                </nav>
                <!-- end NavPanel -->
            </div>

            {% block mainBanner %}
                <div class=\"row\" id=\"mainBanner\">
                    <a href=\"#\">Скидка <span>20%</span> на постельные комплекты VALTERY</a>
                </div>
            {% endblock %}

            {% block bredCrumbs %}
                <div class=\"row\">
                    <ul class=\"crumbs\">
                        <li>
                            <a href=\"index.html\">WebCotton</a>
                            <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
                        </li>
                        <li>
                            <a href=\"index.html\">Корзина покупателя</a>
                            <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
                        </li>
                        <li class=\"active\">
                            <span>Оформление заказа</span>
                        </li>
                    </ul>
                </div>
            {% endblock %}


            <div class=\"row maincontent\">

                {% block content %}
                <div id=\"order\" class=\"col-sm-7 col-xs-12\">
                    <p class=\"heading\">Оформление заказа</p>
                    <form action=\"#\" method=\"post\" role=\"form\">
                        <div class=\"form-group\">
                            <label for=\"order_name\">Фамилия, Имя:*</label>
                            <input type=\"text\" name=\"order_name\" class=\"form-control\" id=\"order_name\"
                                   placeholder=\"Иванов Иван\" required>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_phone\">Телефон:*</label>
                            <input type=\"tel\" name=\"order_phone\" class=\"form-control\" id=\"order_phone\"
                                   placeholder=\"8-918-123-4567, 8-495-123-4567\" required>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_email\">Email:</label>
                            <input type=\"email\" name=\"order_email\" class=\"form-control\" id=\"order_email\"
                                   placeholder=\"adress@domain.ru\">
                        </div>

                        <p class=\"heading\">Доставка*</p>
                        <div class=\"form-group\">
                            <label for=\"order_city\">Населенный пункт:</label>
                            <input type=\"text\" name=\"order_city\" class=\"form-control\" id=\"order_city\"
                                   placeholder=\"Укажите город\"
                                   value=\"Москва\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"order_adress\">Ваш адрес:</label>
                            <input type=\"text\" name=\"order_adress\" class=\"form-control\" id=\"order_adress\"
                                   placeholder=\"ул. Тверская, д. 1, кв. 1\">
                        </div>
                        <div>
                            <label for=\"order_text\">Примечание:</label>
                            <textarea class=\"form-control\" name=\"order_text\" id=\"order_text\" rows=\"3\"
                                      placeholder=\"Домофон, подъезд, дата, время доставки\"></textarea>
                        </div>

                        <p class=\"small\">* отмечены обязательные пункты</p>

                        <input type=\"submit\" class=\"cartsubmit\" href=\"#\" value=\"ОТПРАВИТЬ\">
                    </form>
                </div>

                <div id=\"orderlist\" class=\"col-sm-5 hidden-xs\">
                    <p class=\"heading\">Заказ</p>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                            <img src=\"image/bigImageGood2.jpg\" alt=\"\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <h5>Комплект постельного белья \"Ирис. Слим-сатин гладье 18\" 1.5</h5>
                            <p>Размер: Двухспальное</p>
                            <div class=\"col-sm-7\">
                                <p>Стоимость.:</p>
                                <p>Количество:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p>2500 &#8381;</p>
                                <p>1</p>
                            </div>
                        </div>
                    </div>

                    <div class=\"row orderlist_item\">
                        <div class=\"col-sm-4\">
                        </div>
                        <div class=\"col-sm-8 orderlist_desc row\">
                            <div class=\"col-sm-7\">
                                <p>Итог заказа:</p>
                            </div>
                            <div class=\"col-sm-5\">
                                <p id=\"orderlist_total\">5200 &#8381;</p>
                            </div>
                        </div>
                    </div>
                </div>
                {% endblock %}

            </div>
        </div>

        <footer>
            <div class=\"container\">
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Каталог</p>
                    <ul>
                        <li><a href=\"#\">Постельное белье</a></li>
                        <li><a href=\"#\">Покрывала</a></li>
                        <li><a href=\"#\">Пледы</a></li>
                        <li><a href=\"#\">Одеяла</a></li>
                        <li><a href=\"#\">Подушки</a></li>
                        <li><a href=\"#\">Шторы</a></li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Информация</p>
                    <ul>
                        <li><a href=\"#\">Гарантия</a></li>
                        <li><a href=\"#\">Доставка</a></li>
                        <li><a href=\"#\">Карта сайта</a></li>
                        <li><a href=\"#\">Полезная информация</a></li>
                        <li><a href=\"#\">Политика конфиденциальности</a></li>
                        <li><a href=\"#\">Правила и условия</a></li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Контакты</p>
                    <ul>
                        <li>2016 &copy; Магазин текстиля WebCotton.ru</li>
                        <li>E-mail: info@webcotton.ru</li>
                        <li><abbr title=\"Телефон\">тел.:</abbr> 8 195 555-25-25</li>
                        <li>Режим работы: Без выходных с 10:00 до 20:00.</li>
                    </ul>
                </div>
                <div class=\"col-sm-3 col-xs-6\">
                    <p class=\"heading\">Обратная связь</p>
                    <form role=\"form\">
                        <div class=\"form-group\">
                            <label for=\"InputEmail\">Ваш Email</label>
                            <input type=\"email\" class=\"form-control\" id=\"InputEmail\" placeholder=\"Введите email\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"InputText\">Ваше сообщение</label>
                            <textarea id=\"InputText\" class=\"form-control\" rows=\"3\"></textarea>
                        </div>
                        <button type=\"submit\" class=\"btn btn-default\">Отправить</button>
                    </form>
                </div>
            </div>
        </footer>
    </section>
</main>


<a id=\"toTop\" class=\"btn btn-default\" href=\"/\" role=\"button\">
    <i class=\"glyphicon glyphicon-chevron-up\" aria-hidden=\"true\"></i> Наверх
</a>


<!--Slideout-->
<nav id=\"menu\" class=\"menu\">
    <header class=\"menu-header\">
        <a href=\"#\" class=\"menu-header-title\">WebCotton</a>
    </header>
    <ul class=\"menu-section\">
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Каталог</a>
            <ul class=\"menu-section-first\">
                <li><a href=\"#\">Постельное белье</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                        <li><a href=\"#\">Супер Евро</a></li>
                        <li>
                            <hr>
                        </li>
                        <li><a href=\"#\">Простыни</a></li>
                        <li><a href=\"#\">Наволочки</a></li>
                        <li><a href=\"#\">Пододеяльники</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Покрывала</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Пледы</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Одеяла</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Подушки</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
                <li><a href=\"#\">Шторы</a>
                    <ul class=\"menu-section-second\">
                        <li><a href=\"#\">1.5-спальное</a></li>
                        <li><a href=\"#\">2-спальное</a></li>
                        <li><a href=\"#\">Семейное</a></li>
                        <li><a href=\"#\">Евро</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Доставка</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Оплата</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Статьи</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Правила и условия</a></li>
        <li class=\"menu-section\"><a href=\"#\" class=\"menu-section-title\">Контакты</a></li>
    </ul>
</nav>
<!--Slideout-->

<!-- Scripts -->
<!--<script src=\"js/jquery-3.2.1.min.js\"></script>-->
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js\"></script>
<script src=\"js/bootstrap.min.js\"></script>
<script src=\"js/to_top.js\"></script>
<script src=\"js/slideout.js\"></script>
<script src=\"js/clamp.js\"></script>

<script src=\"js/functions.js\"></script>


<script src=\"js/lightbox.js\"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true
    })
</script>

<!-- FlexSlider -->
<script defer src=\"js/jquery.flexslider.js\"></script>

<script type=\"text/javascript\">
    \$(window).load(function () {
        \$('#carousel').flexslider({
            animation: \"slide\",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 130,
            itemMargin: 5,
            asNavFor: '#slider'
        });

        \$('#slider').flexslider({
            animation: \"slide\",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: \"#carousel\",
            start: function (slider) {
                \$('body').removeClass('loading');
            }
        });
    });
</script>

</body>
</html>", "/templates/site_layout.html.twig", "D:\\USR\\www\\store\\views\\templates\\site_layout.html.twig");
    }
}
