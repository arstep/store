<?php

/* site/base.html */
class __TwigTemplate_58c1fe3a8e746119cba8149c7a3a64c100def9d55fd1d53912a3a6a4c8443aa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
</head>
<body>
render from Twig

<div id=\"content\">
    ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 13
        echo "</div>
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    ";
    }

    public function getTemplateName()
    {
        return "site/base.html";
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  49 => 11,  44 => 5,  38 => 13,  36 => 11,  27 => 5,  21 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/base.html", "D:\\USR\\www\\store\\views\\site\\base.html");
    }
}
