<?php

/* site/category.html.twig */
class __TwigTemplate_f0ce1caae388b19fe1f9902c7bec55e661dfe67489372551fe327e4ed7f17d76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("/templates/site_layout.html.twig", "site/category.html.twig", 1);
        $this->blocks = array(
            'head_title' => array($this, 'block_head_title'),
            'bredCrumbs' => array($this, 'block_bredCrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/templates/site_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "head_title", array(), "array")), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_bredCrumbs($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"row\">
        <ul class=\"crumbs\">
            <li>
                <a href=\"/\">WebCotton</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li class=\"active\">
                <span>";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "name_category", array(), "array"), "html", null, true);
        echo "</span>
            </li>
        </ul>
    </div>
";
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "
    <!--sidebar-->
    <div id=\"leftsidebar\" class=\"col-sm-2\">

        ";
        // line 27
        echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "filter", array());
        echo "

    </div>
    <!--endsidebar-->

    <div class=\"col-sm-10 rightcontent\">

";
        // line 91
        echo "
        <!--sort-->
        <div id=\"list_sort\">
            <div class=\"dropdown\">
                <button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownSort\"
                        data-toggle=\"dropdown\">
                    Сортировать по:&nbsp;
                    <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownSort\">
                    <li><a href=\"javascript:sortByPrice('asc')\">Цене, сначала недорогие</a></li>
                    <li><a href=\"javascript:sortByPrice('desc')\">Цене, сначала дорогие</a></li>
                </ul>
            </div>
            <div class=\"clearfix\"></div>
        </div>
        <!--end sort-->

        <!--products-->
        <div id=\"products_block\">
            <ul id=\"products_list\" class=\"row\">

                ";
        // line 113
        echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "catalog", array());
        echo "

            </ul>

            <div class=\"product_count\">
                <p>Показано 12 из 80 товаров</p>
                <a href=\"#\" class=\"btn btn-default\">Показать еще</a>
            </div>

        </div>
        <!--end og products-->

        <div id=\"article_under_catalog\" class=\"row\">
            <div>
                ";
        // line 127
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "article", array()), "description", array());
        echo "

                <div class=\"splContEdit row\">
                    ";
        // line 130
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "article", array()), "text", array());
        echo "
                </div>

                <a class=\"splLinkEdit\" href=\"\" data-edit=\"Свернуть\">Читать далее..</a>
            </div>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "site/category.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 130,  121 => 127,  104 => 113,  80 => 91,  70 => 27,  64 => 23,  61 => 22,  52 => 15,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '/templates/site_layout.html.twig' %}

{% block head_title %}
    {{ data['head_title'] | capitalize }}
{% endblock %}

{% block bredCrumbs %}
    <div class=\"row\">
        <ul class=\"crumbs\">
            <li>
                <a href=\"/\">WebCotton</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li class=\"active\">
                <span>{{ data['name_category'] }}</span>
            </li>
        </ul>
    </div>
{% endblock %}


{% block content %}

    <!--sidebar-->
    <div id=\"leftsidebar\" class=\"col-sm-2\">

        {{ data.filter | raw }}

    </div>
    <!--endsidebar-->

    <div class=\"col-sm-10 rightcontent\">

{#        <!--submenu-->
        <div id=\"bedding_submenu\">
            <p class=\"heading\">Выберите тип белья</p>
            <ul>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/icon-polutornoe.png\" alt=\"\">
                        <p>Полуторный&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/icon-dvuhspalnoe.png\" alt=\"\">
                        <p>Двухспальный&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/icon-euro.png\" alt=\"\">
                        <p>Евро&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/icon-family.png\" alt=\"\">
                        <p>Семейный&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/icon-kinder.png\" alt=\"\">
                        <p>Детское&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/sheets.png\" alt=\"\">
                        <p>Простыни&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/pillow.png\" alt=\"\">
                        <p>Наволочки&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <img src=\"/image/quilt.gif\" alt=\"\">
                        <p>Пододеяльники&nbsp;<span class=\"badge\">85</span></p>
                    </a>
                </li>
            </ul>
        </div>
        <!--end submenu-->

        <hr>#}

        <!--sort-->
        <div id=\"list_sort\">
            <div class=\"dropdown\">
                <button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownSort\"
                        data-toggle=\"dropdown\">
                    Сортировать по:&nbsp;
                    <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownSort\">
                    <li><a href=\"javascript:sortByPrice('asc')\">Цене, сначала недорогие</a></li>
                    <li><a href=\"javascript:sortByPrice('desc')\">Цене, сначала дорогие</a></li>
                </ul>
            </div>
            <div class=\"clearfix\"></div>
        </div>
        <!--end sort-->

        <!--products-->
        <div id=\"products_block\">
            <ul id=\"products_list\" class=\"row\">

                {{ data.catalog | raw }}

            </ul>

            <div class=\"product_count\">
                <p>Показано 12 из 80 товаров</p>
                <a href=\"#\" class=\"btn btn-default\">Показать еще</a>
            </div>

        </div>
        <!--end og products-->

        <div id=\"article_under_catalog\" class=\"row\">
            <div>
                {{ data.article.description | raw }}

                <div class=\"splContEdit row\">
                    {{ data.article.text | raw}}
                </div>

                <a class=\"splLinkEdit\" href=\"\" data-edit=\"Свернуть\">Читать далее..</a>
            </div>
        </div>

    </div>

{% endblock %}", "site/category.html.twig", "D:\\USR\\www\\store\\views\\site\\category.html.twig");
    }
}
