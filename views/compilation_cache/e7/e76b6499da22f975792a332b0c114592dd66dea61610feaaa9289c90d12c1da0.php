<?php

/* /include/site_left_sidebar_main.html.twig */
class __TwigTemplate_f499aa9103aee49e24eea70e5c72f36aac05b50c4d308739f72d0928cceb4c56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<ul id=\"sidebar_catalog\">

    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->";
    }

    public function getTemplateName()
    {
        return "/include/site_left_sidebar_main.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
<ul id=\"sidebar_catalog\">

    <li>
        <a href=\"postelnoe_belye.html\"><h4 class=\"heading\">Постельное белье</h4></a>
        <h5>По размеру</h5>
        <ul>
            <li><a href=\"#\">1.5-спальное</a></li>
            <li><a href=\"#\">2-спальное</a></li>
            <li><a href=\"#\">Семейное</a></li>
            <li><a href=\"#\">Евро</a></li>
            <li><a href=\"#\">Супер Евро</a></li>
            <li>
                <hr style=\"margin-right: 50px\">
            </li>
            <li><a href=\"#\">Простыни</a></li>
            <li><a href=\"#\">Наволочки</a></li>
            <li><a href=\"#\">Пододеяльники</a></li>
        </ul>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Сатин</a></li>
            <li><a href=\"#\">Полисатин</a></li>
            <li><a href=\"#\">Поплин</a></li>
            <li><a href=\"#\">Бязь</a></li>
            <li><a href=\"#\">Шелк</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Покрывала</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Пледы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Одеяла</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Подушки</h4></a>
        <h5>По наполнителю</h5>
        <ul>
            <li><a href=\"#\">Пух</a></li>
            <li><a href=\"#\">Перо</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>

    <li>
        <a href=\"#\"><h4 class=\"heading\">Шторы</h4></a>
        <h5>По типу ткани</h5>
        <ul>
            <li><a href=\"#\">Шерсть</a></li>
            <li><a href=\"#\">Хлопок</a></li>
            <li><a href=\"#\">Синтетика</a></li>
        </ul>
        <h5>По производителю</h5>
        <ul>
            <li><a href=\"#\">Россия</a></li>
            <li><a href=\"#\">Евросоюз</a></li>
            <li><a href=\"#\">Китай</a></li>
            <li><a href=\"#\">Индия</a></li>
        </ul>
    </li>
</ul>
<!--end of sidebar-->", "/include/site_left_sidebar_main.html.twig", "D:\\USR\\www\\store\\views\\include\\site_left_sidebar_main.html.twig");
    }
}
