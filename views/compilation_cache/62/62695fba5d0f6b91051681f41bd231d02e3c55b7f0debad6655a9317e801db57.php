<?php

/* site/base.tmpl */
class __TwigTemplate_b24e8a878850995e087b075038f993d59c2e686abd19cac1748c4bb6665d5ad2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
</head>
<body>
render from Twig

<div id=\"content\">
    ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 13
        echo "</div>
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    ";
    }

    public function getTemplateName()
    {
        return "site/base.tmpl";
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  49 => 11,  44 => 5,  38 => 13,  36 => 11,  27 => 5,  21 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/base.tmpl", "D:\\USR\\www\\store\\views\\site\\base.tmpl");
    }
}
