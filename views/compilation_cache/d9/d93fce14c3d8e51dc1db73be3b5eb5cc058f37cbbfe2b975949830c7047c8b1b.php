<?php

/* site/base.html.twig */
class __TwigTemplate_735bf24545d2b795ca869e8a901e8654e52814b9fa35d5dd735575b5114291d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
</head>
<body>
render from Twig

<div id=\"content\">
    ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 13
        echo "</div>
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    ";
    }

    public function getTemplateName()
    {
        return "site/base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  49 => 11,  44 => 5,  38 => 13,  36 => 11,  27 => 5,  21 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/base.html.twig", "D:\\USR\\www\\store\\views\\site\\base.html.twig");
    }
}
