<?php

/* include/cart_block.html.twig */
class __TwigTemplate_b223354fc529093ee8d3dcc216aa90dafd78f58de8aa9a2a9c18e24585e34876 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a href=\"javascript:void(0)\">КОРЗИНА (6)</a>
<div id=\"cartinner\">
    <div><p>Товаров в корзине: 6</p></div>
    <div id=\"cartlistgoods\">
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">
                <p class=\"cartnewprice\">3500&#8381;</p>
                <p class=\"cartoldprice\">4300&#8381;</p>
            </div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">1800&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">
                <p class=\"cartnewprice\">2300&#8381;</p>
                <p class=\"cartoldprice\">2700&#8381;</p>
            </div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
    </div>
    <div class=\"itemincart subtotal row\">
        <div>Итог: 10500 &#8381;</div>
    </div>
    <div id=\"cartfooter\">
        <a href=\"shopping_cart.html\">СМОТРЕТЬ</a>
        <a href=\"#\">ОФОРМИТЬ</a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "include/cart_block.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a href=\"javascript:void(0)\">КОРЗИНА (6)</a>
<div id=\"cartinner\">
    <div><p>Товаров в корзине: 6</p></div>
    <div id=\"cartlistgoods\">
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">
                <p class=\"cartnewprice\">3500&#8381;</p>
                <p class=\"cartoldprice\">4300&#8381;</p>
            </div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">1800&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">
                <p class=\"cartnewprice\">2300&#8381;</p>
                <p class=\"cartoldprice\">2700&#8381;</p>
            </div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/mini_img53r.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/pillows.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
        <div class=\"itemincart row\">
            <div class=\"col-xs-3\"><img src=\"image/polot.jpg\" alt=\"\"></div>
            <div class=\"col-xs-6 carttitlegood\"><p>Lorem ipsum dolor sit amet</p>
                <p class=\"cartpricegood\">Количество: 1</p></div>
            <div class=\"col-xs-3\">3500&#8381;</div>
        </div>
    </div>
    <div class=\"itemincart subtotal row\">
        <div>Итог: 10500 &#8381;</div>
    </div>
    <div id=\"cartfooter\">
        <a href=\"shopping_cart.html\">СМОТРЕТЬ</a>
        <a href=\"#\">ОФОРМИТЬ</a>
    </div>
</div>", "include/cart_block.html.twig", "D:\\USR\\www\\store\\views\\include\\cart_block.html.twig");
    }
}
