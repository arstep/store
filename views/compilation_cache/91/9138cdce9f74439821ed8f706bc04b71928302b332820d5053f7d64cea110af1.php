<?php

/* site/product.html.twig */
class __TwigTemplate_542d1d71c4424e0825bcb335e3a32de77d8da76a6674ed044f3d9f674e88b88a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("/templates/site_layout.html.twig", "site/product.html.twig", 1);
        $this->blocks = array(
            'head_title' => array($this, 'block_head_title'),
            'bredCrumbs' => array($this, 'block_bredCrumbs'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/templates/site_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "head_title", array(), "array")), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_bredCrumbs($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"row\">
        <ul class=\"crumbs\">
            <li>
                <a href=\"/\">WebCotton</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li>
                <a href=\"/catalog/";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "url_category", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "name_category", array()), "html", null, true);
        echo "</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li class=\"active\">
                <span>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "name", array()), "html", null, true);
        echo "</span>
            </li>
        </ul>
    </div>
";
    }

    // line 26
    public function block_content($context, array $blocks = array())
    {
        // line 27
        echo "
    <div id=\"slider_product\" class=\"col-sm-7\">
        <section class=\"slider\">
            <div id=\"slider\" class=\"flexslider\">
                <ul class=\"slides\">

                    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "photo", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 34
            echo "
                        <li>
                            <a href=\"/";
            // line 36
            echo twig_escape_filter($this->env, ($context["pathPhotoGoods"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "filename", array()), "html", null, true);
            echo "\"
                               data-lightbox=\"productfoto\">
                                <img src=\"/";
            // line 38
            echo twig_escape_filter($this->env, ($context["pathPhotoGoods"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "filename", array()), "html", null, true);
            echo "\"/>
                            </a>
                        </li>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
                </ul>
            </div>
            <div id=\"carousel\" class=\"flexslider\">
                <ul class=\"slides\">

                    ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "photo", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 50
            echo "
                        <li>
                            <img src=\"/";
            // line 52
            echo twig_escape_filter($this->env, ($context["pathPhotoGoods"] ?? null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["item"], "filename", array()), "html", null, true);
            echo "\"/>
                        </li>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "
                </ul>
            </div>
        </section>
    </div>

    <div id=\"info_product\" class=\"col-sm-5\">

        ";
        // line 64
        echo ($context["info_product"] ?? null);
        echo "

    </div>

    <div class=\"clearfix\"></div>

    <div id=\"product_description\">
        <h5 class=\"heading\">Описание</h5>
        <p>
            ";
        // line 73
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "product", array()), "description", array());
        echo "
        </p>
        <h5 class=\"heading\">Примечание</h5>
        <p>
            На сайте представлена информация о товаре, заявленная товаропроизводителем. Цвет изделия
            может отличаться от изображения в зависимости от настроек Вашего монитора. Данное
            несоответствие не расценивается как брак.
        </p>
    </div>

";
    }

    public function getTemplateName()
    {
        return "site/product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 73,  147 => 64,  137 => 56,  125 => 52,  121 => 50,  117 => 49,  109 => 43,  96 => 38,  89 => 36,  85 => 34,  81 => 33,  73 => 27,  70 => 26,  61 => 19,  52 => 15,  43 => 8,  40 => 7,  33 => 4,  30 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '/templates/site_layout.html.twig' %}

{% block head_title %}
    {{ data['head_title'] | capitalize }}
{% endblock %}

{% block bredCrumbs %}
    <div class=\"row\">
        <ul class=\"crumbs\">
            <li>
                <a href=\"/\">WebCotton</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li>
                <a href=\"/catalog/{{ data.url_category }}\">{{ data.name_category }}</a>
                <i class=\"glyphicon glyphicon-menu-right\" aria-hidden=\"true\" style=\"font-size: 0.7em\"></i>
            </li>
            <li class=\"active\">
                <span>{{ data.product.name }}</span>
            </li>
        </ul>
    </div>
{% endblock %}


{% block content %}

    <div id=\"slider_product\" class=\"col-sm-7\">
        <section class=\"slider\">
            <div id=\"slider\" class=\"flexslider\">
                <ul class=\"slides\">

                    {% for item in data.product.photo %}

                        <li>
                            <a href=\"/{{ pathPhotoGoods }}/{{ item.filename }}\"
                               data-lightbox=\"productfoto\">
                                <img src=\"/{{ pathPhotoGoods }}/{{ item.filename }}\"/>
                            </a>
                        </li>

                    {% endfor %}

                </ul>
            </div>
            <div id=\"carousel\" class=\"flexslider\">
                <ul class=\"slides\">

                    {% for item in data.product.photo %}

                        <li>
                            <img src=\"/{{ pathPhotoGoods }}/{{ item.filename }}\"/>
                        </li>

                    {% endfor %}

                </ul>
            </div>
        </section>
    </div>

    <div id=\"info_product\" class=\"col-sm-5\">

        {{ info_product | raw }}

    </div>

    <div class=\"clearfix\"></div>

    <div id=\"product_description\">
        <h5 class=\"heading\">Описание</h5>
        <p>
            {{ data.product.description | raw }}
        </p>
        <h5 class=\"heading\">Примечание</h5>
        <p>
            На сайте представлена информация о товаре, заявленная товаропроизводителем. Цвет изделия
            может отличаться от изображения в зависимости от настроек Вашего монитора. Данное
            несоответствие не расценивается как брак.
        </p>
    </div>

{% endblock %}", "site/product.html.twig", "D:\\USR\\www\\store\\views\\site\\product.html.twig");
    }
}
